FROM cern/cs8-base

LABEL maintainer="https://gitlab.cern.ch/hse-see-co-docker/openshift-java-s2i-java17"

ENV GRADLE_VERSION=7.3.2 \
    APN_VERSION=1.2.31

LABEL io.k8s.description="Platform for building and running Java 17 Applications" \
    io.k8s.display-name="Java 17 (Gradle)" \
    io.openshift.expose-services="8080:http" \
    io.openshift.tags="builder,java,java17,gradle,springboot" \
    io.openshift.s2i.destination="/opt/s2i/destination" \
    io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

RUN INSTALL_PKGS="gcc tar unzip bc which lsof java-17-openjdk-devel.x86_64 CERN-CA-certs apr-devel openssl-devel cern-get-sso-cookie krb5-workstation" && \
    yum install -y $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all -y

ENV HOME=${HOME:-/opt/app-root/src}
ENV JAVA_HOME="/usr/lib/jvm/java-17-openjdk"

WORKDIR $HOME

RUN curl -sL -0 https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip -o /tmp/gradle-${GRADLE_VERSION}-bin.zip && \
    unzip /tmp/gradle-${GRADLE_VERSION}-bin.zip -d /usr/local/ && \
    rm /tmp/gradle-${GRADLE_VERSION}-bin.zip && \
    mv /usr/local/gradle-${GRADLE_VERSION} /usr/local/gradle && \
    ln -sf /usr/local/gradle/bin/gradle /usr/local/bin/gradle

# Make dir for s2i
RUN mkdir -p /opt/s2i/destination && mkdir -p $HOME/.ssh

# Install tomcat connectors
RUN curl https://downloads.apache.org/tomcat/tomcat-connectors/native/${APN_VERSION}/source/tomcat-native-${APN_VERSION}-src.tar.gz -o tomcat-native.tar.gz && \
    tar -xvzf tomcat-native.tar.gz && \
    cd tomcat-native-${APN_VERSION}-src/native && \
    chmod +x configure && \
    ./configure --with-apr=/usr/bin/apr-1-config \
            --with-java-home=${JAVA_HOME} \
            --with-ssl=yes \
            --prefix=${HOME}/apache-tomcat && \
    make && make install

ADD ./settings/settings.xml $HOME/.m2/
ADD ./settings/init.gradle $HOME/.gradle/
ADD ./settings/.ssh/config $HOME/.ssh/config

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH
COPY ./s2i/bin/ /usr/libexec/s2i

RUN chown -R 1001:1001 /usr/libexec/s2i
RUN chmod -R ug+rwx /usr/libexec/s2i

RUN chown -R 1001:1001 $HOME && \
    chmod -R ug+rw /opt/s2i/destination && \
    chmod -R ug+r $HOME/.ssh && \
    chown -R 1001:1001 /opt/app-root

# TODO: Update keytool usage Warning: use -cacerts option to access cacerts keystore
RUN keytool -import -noprompt -trustcacerts -alias "CERN Root Certification Authority 2" -file "/etc/pki/tls/certs/CERN_Root_Certification_Authority_2.crt" -keystore "$JAVA_HOME/lib/security/cacerts" --storepass changeit && \
    keytool -import -noprompt -trustcacerts -alias "CERN Certification Authority" -file "/etc/pki/tls/certs/CERN_Certification_Authority.crt" -keystore "$JAVA_HOME/lib/security/cacerts" --storepass changeit && \
    keytool -import -noprompt -trustcacerts -alias "CERN Certification Authority(1)" -file "/etc/pki/tls/certs/CERN_Certification_Authority(1).crt" -keystore "$JAVA_HOME/lib/security/cacerts" --storepass changeit

ADD krb5.conf /etc/krb5.conf
RUN chmod +r /etc/krb5.conf
USER 1001

EXPOSE 8080

CMD ["/usr/libexec/s2i/usage"]